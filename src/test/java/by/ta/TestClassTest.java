package by.ta;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class TestClassTest {

    @Test
    public void test() {
        String test = "test";
        TestClass testClass = new TestClass();
        assertEquals(testClass.getString(test), "s");
    }
}
